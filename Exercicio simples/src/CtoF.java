import java.util.Scanner;

/**
 * 
 * @author Victor Reis (2017101820)
 *
 */

public class CtoF {
	
	static double c, f;
	static Scanner temp = new Scanner (System.in);

	public static double conversao() {
		return (c*9/5) + 32;
	}
	
	public static void main(String[] args) {
		
		c=0;
		f=0;
		
		System.out.print("Digite a temperatura em Celsius: ");
		c= temp.nextDouble();
		
		f= conversao();
				
		System.out.println("A temperatura em Fahrenheit � " + f);
	}

}
