import java.util.Scanner;

/**
 * 
 * @author Victor Reis (2017101820)
 *
 */
public class Cozinha {
	
	static double comp, larg, alt, area;
	static int caixas;
	static Scanner conta= new Scanner(System.in);
	
	public static double area_c() {
		return (comp*alt*2) + (larg*alt*2);
	}

	public static int caixas_c() {
		return (int) (area/1.5);
	}
	
	public static void main(String[] args) {
		
		System.out.print("Qual o comprimento da cozinha? ");
		comp= conta.nextDouble();
		System.out.print("Qual a largura da cozinha? ");
		larg= conta.nextDouble();
		System.out.print("Qual a altura da cozinha? ");
		alt= conta.nextDouble();
		
		area = area_c();
		caixas = caixas_c();
		
		System.out.println("Quantidade de caixas de azulejos para colocar em todas as paredes � " + caixas);
		
	}

}
