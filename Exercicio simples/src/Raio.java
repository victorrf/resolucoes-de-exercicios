import java.util.Scanner;

/**
 * 
 * @author Victor Reis (2017101820)
 *
 */

public class Raio {
	
	static double r, a;
	static Scanner conta = new Scanner(System.in);
	
	public static double area() {
		return 3.14*r*r; //Pi = 3.14
	}

	public static void main(String[] args) {
		
		r=0;
		a=0;
		
		System.out.print("Digite o raio do circulo: ");
		r= conta.nextDouble();
		
		a= area();
		
		System.out.println("A area desse circulo � " + a +"cm�.");

	}

}
