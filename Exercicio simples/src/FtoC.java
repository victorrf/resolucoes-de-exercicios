import java.util.Scanner;

/**
 * 
 * @author Victor Reis (2017101820)
 *
 */

public class FtoC {
	
	static double f, c;
	static Scanner temp = new Scanner (System.in);
	
	public static double conversao() {
		return (f-32)*5/9;
	}

	public static void main(String[] args) {
		
		f=0;
		c=0;
		
		System.out.print("Digite a temperatura em Fahrenheit: ");
		f= temp.nextDouble();
		
		c= conversao();
		
		System.out.println("A temperatura em Celsius � " + c);

	}

}
