import java.util.Scanner;

/**
 * 
 * @author Victor Reis (2017101820)
 *
 */

/*{	pot_lamp: pot�ncia da l�mpada 
	larg_com: largura do c�modo
	comp_com: comprimento do c�modo
	area_com: �rea do c�modo
	pot_total: pot�ncia total
	num_lamp: n�mero de l�mpada}*/

public class Lampada {
	
	static double pot_lamp, larg_com, comp_com, area_com, pot_total;
	static int num_lamp;
	static Scanner conta = new Scanner(System.in);
	
	public static double area() {
		return larg_com * comp_com;
	}
	
	public static double total() {
		return area_com * 18;
	}
	
	public static int numero() {
		return (int) (pot_total/pot_lamp);
	}

	public static void main(String[] args) {

		System.out.print("Qual a pot�ncia da l�mpada (em watts)? ");
		pot_lamp= conta.nextDouble();
		System.out.print("Qual a largura do c�modo (em metros)? ");
		larg_com= conta.nextDouble();
		System.out.print("Qual o comprimento do c�modo (em metros)? ");
		comp_com= conta.nextDouble();
		
		area_com= area();
		pot_total= total();
		num_lamp= numero();

		System.out.println("N�mero de l�mpadas necess�rias para iluminar esse c�modo: " + num_lamp);
	}

}
