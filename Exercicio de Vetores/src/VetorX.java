

/**
 * @author Victor Reis (2017101820)
 *
 */

public class VetorX {

	static int x [] = new int [10];
	static int i;
	
	public static void main(String[] args) {
		
		for(i=0; i<10; i++) {
			x[i] = 30;
		}
		
		for(i=0; i<10; i++) {
			System.out.println(i+1 + " - " + x[i]);
		}
	}
}
