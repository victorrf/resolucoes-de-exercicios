

/**
 * @author Victor Reis (2017101820)
 *
 */

public class Vetor {

	static int v [] = {5, 1, 4, 2, 7, 8, 3, 6};
	static int i, aux=0;

	public static void main(String[] args) {
		
		System.out.println("------------------Antes--------------------");
		
		for (i=0; i<8; i++) {
			System.out.println(i+1 + " - " + v[i]);
		}

		for (i=7; i >= 4; i--) {
			aux = v[i];
			v[i] = v[7 - i];
			v[7 - i] = aux;
		}
		
		v[2] = v[0];
		v[v[2]] = v[v[1]];
		
		System.out.println("------------------Depois--------------------");
		
		for (i=0; i<8; i++) {
			System.out.println(i+1 + " - " + v[i]);
		}
	}

}
