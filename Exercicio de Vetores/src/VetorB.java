

/**
 * @author Victor Reis (2017101820) 
 *
 */
public class VetorB {

	static int b [] = new int [10];
	static int i;

	public static void main(String[] args) {

		for(i=0; i<10; i++) {
			if(i%2 == 0) {
				b[i] = 0;
			}else {
				b[i] = 1;
			}
		}
		
		for(i=0; i<10; i++) {
			System.out.println(i + " - " + b[i]);
		}

	}

}
