

/** 
 * @author Victor Reis (2017101820) 
 *
 */
public class VetorA {
	
	static int a [] = new int [10];
	static int i;

	public static void main(String[] args) {

		for(i=0; i<10; i++) {
			a[i] = 1+i;
		}
		
		for(i=0; i<10; i++) {
			System.out.println(i+1 + " - " + a[i]);
		}
		
	}

}
