import java.util.Scanner;

/**
 * 
 * @author Victor Reis (2017101820)
 *
 */

public class Nota {
	
	static double not1, not2, media;
	static Scanner conta = new Scanner (System.in);
	
	public static double media_n() {
		return (not1 + not2)/2;
	}

public static void main(String[] args) {
		
		System.out.println("Digite a primeira nota: ");
		not1= conta.nextDouble();
		
		while ((not1 < 0) || (not1 > 10)) {
			if ((not1 < 0) || (not1 > 10)) {
				System.out.println("Nota Invalida!");
			}
			System.out.println("Digite a primeira nota: ");
			not1= conta.nextDouble();
		}
		
		System.out.println("Digite a segunda nota: ");
		not2= conta.nextDouble();
		
		while ((not2 < 0) || (not2 > 10)) {
			if ((not2 < 0) || (not2 > 10)) {
				System.out.println("Nota invalida!");
			}
			System.out.println("Digite a segunda nota: ");
			not2= conta.nextDouble();
		}
		
		media = media_n();
		System.out.println("Media semestral: " + media);

	}

}
