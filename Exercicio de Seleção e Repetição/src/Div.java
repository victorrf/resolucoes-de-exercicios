import java.util.Scanner;

/**
 * 
 * @author Victor Reis (2017101820)
 *
 */

public class Div {
	
	static int a, b;
	static double div;
	static Scanner conta = new Scanner(System.in);
	
	public static double divisao() {
		return a/b;
	}

	public static void main(String[] args) {
		
		a=0;
		b=0;
		div=0;
		
		System.out.println("Digite um valor: ");
		a= conta.nextInt();
		
		while (b == 0) {
			System.out.println("Digite outro valor: ");
			b= conta.nextInt();
			if (b == 0) {
				System.out.println("Valor inv�lido!");
			}
		}
		
		div= divisao();
		
		System.out.println("A divisao do primeiro valor com segundo eh: " + div);
		
	}

}
