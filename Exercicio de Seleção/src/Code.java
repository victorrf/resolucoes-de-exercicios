import java.util.Scanner;

/**
 * 
 * @author Victor Reis (2017101820)
 *
 */

public class Code {
	
	static Scanner num = new Scanner (System.in);

	public static void main(String[] args) {
		int cod;
		
		System.out.println("Qual o codigo do Produto? ");
		cod = num.nextInt();
		
		if (cod == 1) {
			System.out.println("Sul");
		}else if (cod == 2) {
			System.out.println("Norte");
		}else if (cod == 3) {
			System.out.println("Leste");
		}else if (cod == 4) {
			System.out.println("Oeste");
		}else if ((cod == 5) || (cod == 6)) {
			System.out.println("Nordeste");
		}else if ((cod == 7) || (cod == 8) || (cod == 9)) {
			System.out.println("Sudeste");
		}else if (cod == 10) {
			System.out.println("Centro-Oeste");
		}else if (cod == 11) {
			System.out.println("Noroeste");
		}else {
			System.out.println("Importado");
		}

	}
	
}
