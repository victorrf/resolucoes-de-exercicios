import java.util.Scanner;

/**
 * 
 * @author Victor Reis (2017101820)
 *
 */

public class Exame {
	
	static Scanner conta = new Scanner(System.in);

	public static void main(String[] args) {
		
		double av1, av2, av3, media;
		
		System.out.println("Digite a nota da AV1: ");
		av1= conta.nextDouble();
		System.out.println("Digite a nota da AV2: ");
		av2= conta.nextDouble();
		System.out.println("Digite a nota da AV3: ");
		av3= conta.nextDouble();
		
		if ((av1 > av3) && (av2 > av3)) {
			media= (av1+av2)/2;
		}else if ((av1 > av2) && (av3 > av2)) {
			media= (av1+av3)/2;
		}else {
			media= (av2+av3)/2;
		}
		
		if (media >= 6) {
			System.out.println("Aprovado com " + media + ", parabens!");
		}else if (media < 3) {
			System.out.println("Reprovado com " + media + ", va estudar!");
		}else {
			System.out.println("No Exame ainda com " + media);
		}
	}

}
