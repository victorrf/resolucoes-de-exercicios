import java.util.Scanner;

/**
 * 
 * @author Victor Reis (2017101820)
 *
 */

public class Valor {
	
	static Scanner sinal = new Scanner (System.in);

public static void main(String[] args) {
		
		double valor;
		
		System.out.println("Digite um valor: ");
		valor= sinal.nextDouble();
		
		if (valor < 0) {
			System.out.println("O valor " + valor + " � negativo");
		}else if (valor > 0) {
			System.out.println("O valor " + valor + " � positivo");
		}else {
			System.out.println("O valor " + valor + " � neutro");
		}

	}

}
