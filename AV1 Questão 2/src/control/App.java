package control;

import java.util.Scanner;
import model.ContaFisica;
import model.ContaJuridica;

/**
 * @author Victor
 *
 */

public class App {
	
	static final int limite = 3;
	
	static int index = 0;
	static int tipo;
	static int tipo2;
	
	static ContaFisica [] listacf = new ContaFisica [limite];
	static ContaJuridica [] listacj = new ContaJuridica [limite];
	
	static Scanner tecla = new Scanner (System.in);

	
	public static void main(String[] args) {
		
		int op;
		do {
			System.out.println("*****MENU PRINCIPAL*****");
			System.out.println("1-Incluir Conta");
			System.out.println("2-Excluir Conta");
			System.out.println("3-Debitar Conta");
			System.out.println("4-Creditar Conta");
			System.out.println("5-Consultar Conta");
			System.out.println("6-Transferir Conta");
			System.out.println("7-Sair");
			op = tecla.nextInt();
			tecla.nextLine();
			switch (op) {
				case 1: incluirConta(); break;
				case 2: excluirConta(); break;
				case 3: debitarConta(); break;
				case 4: creditarConta(); break;
				case 5: consultarConta(); break;
				case 6: transferirConta(); break;
				case 7: break;
			}
		} while (op != 7);
		
		if(op == 7) {
			System.out.println("Sess�o encerrada, at� a pr�xima!");
		}
		

	}
	
	public static void incluirConta() {
		System.out.println("Digite qual tipo de conta: 1- Pessoa Fisica / 2 - Pessoa Juridica");
		tipo = tecla.nextInt();
		
		if(tipo == 1) {
			System.out.println("Digite o nome do proprietario da conta:");
			String name = tecla.nextLine();
			System.out.println("Digite o numero da conta:");
			long num = tecla.nextLong();
			System.out.println("Digite o saldo da conta:");
			double sal = tecla.nextDouble();
			System.out.println("Digite o CPF do proprietario da conta:");
			long cpf = tecla.nextLong();
			
			listacf [index++] = new ContaFisica(name, num, sal, cpf);
			System.out.println("Conta de pessoa fisica criada com sucesso!");
			
		}else if (tipo == 2) {
			System.out.println("Digite o nome da empresa da conta:");
			String name = tecla.nextLine();
			System.out.println("Digite o numero da conta:");
			long num = tecla.nextLong();
			System.out.println("Digite o saldo da conta:");
			double sal = tecla.nextDouble();
			System.out.println("Digite o CNPJ da empresa da conta:");
			long cnpj = tecla.nextLong();
			
			listacj [index++] = new ContaJuridica(name, num, sal, cnpj);
			System.out.println("Conta de pessoa juridica criada com sucesso!");
		}
		
		tipo = 0;
		
	}
	
	public static void excluirConta() {
		System.out.println("Digite qual tipo de conta: 1- Pessoa Fisica / 2 - Pessoa Juridica");
		tipo = tecla.nextInt();
		
		if(tipo == 1) {
			System.out.println("Digite o numero da conta:");
			long num = tecla.nextLong();
			
			for (int i = 0; i < listacf.length-1; i++) {
				if(num == listacf[i].getNumero()) {
					listacf[i] = null;
					System.out.println("Conta excluida com sucesso!");
					break;
				}else {
					System.out.println("Conta n�o existe!");
					break;
				}
			}
			
		}else if (tipo == 2) {
			System.out.println("Digite o numero da conta:");
			long num = tecla.nextLong();
			
			for (int i = 0; i < listacj.length-1; i++) {
				if(num == listacj[i].getNumero()) {
					listacj[i] = null;
					System.out.println("Conta excluida com sucesso!");
					break;
				}else {
					System.out.println("Conta n�o existe!");
					break;
				}
			}

			
		}
		
		tipo = 0;
	}
	
	public static void debitarConta() {
		System.out.println("Digite qual tipo de conta: 1- Pessoa Fisica / 2 - Pessoa Juridica");
		tipo = tecla.nextInt();
		
		if(tipo == 1) {
			System.out.println("Digite o numero da conta:");
			long num = tecla.nextLong();
			System.out.println("Digite a soma do debito:");
			double soma = tecla.nextDouble();
			
			for (int i = 0; i < listacf.length-1; i++) {
				if(num == listacf[i].getNumero()) {
					listacf[i].debitar(soma);
					System.out.println("Debito em conta com sucesso!");
					break;
				}else {
					System.out.println("Conta n�o existe!");
					break;
				}
			}

			
		}else if (tipo == 2) {
			System.out.println("Digite o numero da conta:");
			long num = tecla.nextLong();
			System.out.println("Digite a soma do debito:");
			double soma = tecla.nextDouble();
			
			for (int i = 0; i < listacj.length-1; i++) {
				if(num == listacj[i].getNumero()) {
					listacj[i].debitar(soma);
					System.out.println("Debito em conta com sucesso!");
					break;
				}else {
					System.out.println("Conta n�o existe!");
					break;
				}
			}
		}
		
		tipo = 0;
	}
	
	public static void creditarConta() {
		System.out.println("Digite qual tipo de conta: 1- Pessoa Fisica / 2 - Pessoa Juridica");
		tipo = tecla.nextInt();
		
		if(tipo == 1) {
			System.out.println("Digite o numero da conta:");
			long num = tecla.nextLong();
			System.out.println("Digite a soma do credito:");
			double soma = tecla.nextDouble();
			
			for (int i = 0; i < listacf.length-1; i++) {
				if(num == listacf[i].getNumero()) {
					listacf[i].creditar(soma);
					System.out.println("Credito em conta com sucesso!");
					break;
				}else {
					System.out.println("Conta n�o existe!");
					break;
				}
			}

			
		}else if (tipo == 2) {
			System.out.println("Digite o numero da conta:");
			long num = tecla.nextLong();
			System.out.println("Digite a soma do credito:");
			double soma = tecla.nextDouble();
			
			for (int i = 0; i < listacj.length-1; i++) {
				if(num == listacj[i].getNumero()) {
					listacj[i].creditar(soma);
					System.out.println("Credito em conta com sucesso!");
					break;
				}else {
					System.out.println("Conta n�o existe!");
					break;
				}
			}
		}
		
		tipo = 0;
		
	}
	
	public static void consultarConta() {
		System.out.println("Digite qual tipo de conta: 1- Pessoa Fisica / 2 - Pessoa Juridica");
		tipo = tecla.nextInt();
		
		if(tipo == 1) {
			System.out.println("Digite o numero da conta:");
			long num = tecla.nextLong();
			
			for (int i = 0; i < listacf.length-1; i++) {
				if(num == listacf[i].getNumero()) {
					System.out.println("Nome:..........Numero:..........Saldo:..........CPF:");
					System.out.println(listacf[i].getNome() + "....." +
									listacf[i].getNumero() + "....." +
									listacf[i].getSaldo() + "....." +
									listacf[i].getCpf());
					break;
				}else {
					System.out.println("Conta n�o existe!");
					break;
				}
			}

			
		}else if (tipo == 2) {
			System.out.println("Digite o numero da conta:");
			long num = tecla.nextLong();
			
			for (int i = 0; i < listacj.length-1; i++) {
				if(num == listacj[i].getNumero()) {
					System.out.println("Nome:..........Numero:..........Saldo:..........CNPJ:");
					System.out.println(listacj[i].getNome() + "....." +
									listacj[i].getNumero() + "....." +
									listacj[i].getSaldo() + "....." +
									listacj[i].getCnpj());
					break;
				}else {
					System.out.println("Conta n�o existe!");
					break;
				}
			}
		}
		
		tipo = 0;
	}

	public static void transferirConta() {
		System.out.println("Digite qual tipo de conta: 1- Pessoa Fisica / 2 - Pessoa Juridica");
		tipo = tecla.nextInt();
		
		if(tipo == 1) {
			System.out.println("Digite o numero da conta:");
			long num = tecla.nextLong();
			System.out.println("Digite a soma da transferencia:");
			double soma = tecla.nextDouble();
			
			for (int i = 0; i < listacf.length-1; i++) {
				if(num == listacf[i].getNumero()) {
					listacf[i].trans_saida(soma);
					
					System.out.println("Digite qual tipo de conta: 1- Pessoa Fisica / 2 - Pessoa Juridica");
					tipo2 = tecla.nextInt();
					
					if (tipo2 == 1) {
						for (int j = 0; j < listacf.length-1; j++) {
							if(num == listacf[j].getNumero()) {
								System.out.println("Digite o numero da conta:");
								long n = tecla.nextLong();
								listacf[j].trans_entrada(soma);
								break;
							}else {
								System.out.println("Conta n�o existe!");
								break;
							}
						}
					}else if (tipo2 == 2) {
						for (int j = 0; j < listacj.length-1; j++) {
							if(num == listacj[j].getNumero()) {
								System.out.println("Digite o numero da conta:");
								long n = tecla.nextLong();
								listacj[j].trans_entrada(soma);
								break;
							}else {
								System.out.println("Conta n�o existe!");
								break;
							}
						}
					}
				
					System.out.println("Transferencia entre contas realizadas com sucesso!");
					break;
				}else {
					System.out.println("Conta n�o existe!");
					break;
				}
			}

			
		}else if (tipo == 2) {
			System.out.println("Digite o numero da conta:");
			long num = tecla.nextLong();
			System.out.println("Digite a soma da transferencia:");
			double soma = tecla.nextDouble();
			
			for (int i = 0; i < listacj.length-1; i++) {
				if(num == listacj[i].getNumero()) {
					listacj[i].trans_saida(soma);
					
					System.out.println("Digite qual tipo de conta: 1- Pessoa Fisica / 2 - Pessoa Juridica");
					tipo2 = tecla.nextInt();
					
					if (tipo2 == 1) {
						for (int j = 0; j < listacf.length-1; j++) {
							if(num == listacf[j].getNumero()) {
								System.out.println("Digite o numero da conta:");
								long n = tecla.nextLong();
								listacf[j].trans_entrada(soma);
								break;
							}else {
								System.out.println("Conta n�o existe!");
								break;
							}
						}
					}else if (tipo2 == 2) {
						for (int j = 0; j < listacj.length-1; j++) {
							if(num == listacj[j].getNumero()) {
								System.out.println("Digite o numero da conta:");
								long n = tecla.nextLong();
								listacj[j].trans_entrada(soma);
								break;
							}else {
								System.out.println("Conta n�o existe!");
								break;
							}
						}
					}
				
					System.out.println("Transferencia entre contas realizadas com sucesso!");
					break;
				}else {
					System.out.println("Conta n�o existe!");
					break;
				}
			}

			
		}
		
		tipo = 0;
		tipo2 = 0;	
	}
	
}


