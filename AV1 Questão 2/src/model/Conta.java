package model;

/**
 * @author Victor Reis (2017101820)
 *
 */

public class Conta {
	
	private String nome;
	private long numero;
	private double saldo;
	
	public Conta (String nome, long numero, double saldo) {
		this.nome = nome;
		this.numero = numero;
		this.saldo = saldo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public long getNumero() {
		return numero;
	}
	
	public void setNumero(long numero) {
		this.numero = numero;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	public void debitar(double soma) {
		saldo -= soma;
	}
	
	public void creditar(double soma) {
		saldo += soma;
	}
	
	public void trans_saida(double soma) {
		saldo -= soma;
	}
	
	public void trans_entrada(double soma) {
		saldo += soma;
	}

}
