package model;

/**
 * @author Victor
 *
 */

public class ContaJuridica extends Conta {
	
	private long cnpj;
	
	public ContaJuridica (String name, long num, double sal, long cnpj) {
		super(name, num, sal);
		this.cnpj = cnpj;
	}
	
	public long getCnpj() {
		return cnpj;
	}
	
	public void setCnpj(long cnpj) {
		this.cnpj = cnpj;
	}
	
	public void debitar(double soma) {
		setSaldo(getSaldo() - soma * 1.2);
	}
	
	

}
