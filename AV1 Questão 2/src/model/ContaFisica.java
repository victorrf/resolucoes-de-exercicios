package model;

/**
 * @author Victor Reis (2017101820)
 *
 */

public class ContaFisica extends Conta {
	
	private long cpf;
	
	public ContaFisica(String name, long num, double sal, long cpf) {
		super(name, num, sal);
		this.cpf = cpf;
	}
	
	
	public long getCpf() {
		return cpf;
	}
	
	public void setCpf(long cpf) {
		this.cpf = cpf;
	}

}
