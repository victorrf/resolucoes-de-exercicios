import java.util.Scanner;


/**
 * @author Victor Reis (20117101820)
 *
 */

public class App {
	
	static final int maxlist = 5;
	
	static int index = 0;
	
	static Pessoa[] lista = new Pessoa[maxlist];
	
	static Scanner tecla = new Scanner (System.in);
	
	
	public static void main(String[] args) {
		int op;
		do {
			System.out.println("****MENU PRINCIPAL****");
			System.out.println("1-Incluir pessoa");
			System.out.println("2-Listar pessoas");
			System.out.println("3-Sair");
			op = tecla.nextInt();
			tecla.nextLine();
			switch (op) {
				case 1: incluirPessoa(); break;
				case 2: listarPessoas(); break;
				case 3: break;
			}
		} while (op !=3);
		
		if(op == 3) {
			System.out.println("Sess�o encerrada, at� a pr�xima!");
		}
		
	}
	
	
		
		public static void incluirPessoa() {
			System.out.println("Digite o nome da pessoa:");
			String nome = tecla.nextLine();
			System.out.println("Digite o cpf da pessoa:");
			long cpf = tecla.nextLong();
			tecla.nextLine();
			System.out.println("Digite a data de nascimento da pessoa:");
			String data_nasc = tecla.nextLine();
		
			lista[index++] = new Pessoa(nome, cpf, data_nasc);
			
			System.out.println("Pessoa registrada com sucesso!");
		}
		
		public static void listarPessoas() {
			System.out.println("Nome:..........CPF:..........Data de Nascimento:");
			for (int i=0; i<lista.length-1; i++) {
				if (lista[i] !=null) {
					System.out.println(lista[i].getNome() + ".........." + 
										lista[i].getCpf() + ".........." + 
										lista[i].getData_nasc());
				}else
					break;
			}
		}
	

}
