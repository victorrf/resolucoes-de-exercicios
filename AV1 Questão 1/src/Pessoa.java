

/**
 * @author Victor Reis (20117101820)
 *
 */

public class Pessoa {
	
	private String nome;
	private long cpf;
	private String data_nasc;
	

	public String getNome () {
		return nome;
	}
	
	public void setNome (String nome) {
		this.nome = nome;
	}
	
	public long getCpf () {
		return cpf;
	}
	
	public void setCpf (long cpf) {
		this.cpf = cpf;
	}
	
	public String getData_nasc () {
		return data_nasc;
	}
	
	public void setData_nasc (String data_nasc) {
		this.data_nasc = data_nasc;
	}

	public Pessoa (String nome, long cpf, String data_nasc) {
		this.nome = nome;
		this.cpf = cpf;
		this.data_nasc = data_nasc;
	}
}
