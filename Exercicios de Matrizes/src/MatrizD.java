import java.util.Scanner;

/**
 * @author Victor Reis (2017101820)
 *
 */
public class MatrizD {

	static int i, j, x, teste, op;
	static int d [][] = new int[3][3];
	static Scanner valor = new Scanner (System.in);

	public static void main(String[] args) {
		
		teste = 0;
		
		for(i=0; i < 3; i++) {
			for(j=0; j < 3; j++) {
				System.out.println("Digite um numero inteiro na Matriz D 3x3: ");
				d[i][j] = valor.nextInt();
			}
		}
		
		op = 1;
		while(op == 1) {
			teste = 0;
			System.out.println("Digite um numero inteiro para verificar se existe na matriz D: ");
			x = valor.nextInt();
			for(i=0; i < 3; i++) {
				for(j=0; j < 3; j++) {
					if(x == d[i][j]) {
						teste = 1;
					}
				}
			}
			if(teste == 1) {
				System.out.println("Este numero existe na matriz D");
				}
				else {
					System.out.println("Este numero nao existe na matriz D");
				}
			
			System.out.println("Deseja continuar: 1/SIM ou 2/NAO");
			op = valor.nextInt();
		}

	}

}
