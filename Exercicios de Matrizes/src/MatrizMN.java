

/**
 * @author Victor Reis (2017101820)
 *
 */

public class MatrizMN {

	static int m [][] = new int [3][4];
	static char n [][] = new char [2][2];
	static int i, j;

	public static void main(String[] args) {
		
		System.out.println("--------Matriz 3x4--------");
		
		j = 1;
		for(i=0; i < 3; i++) {
			m[i][j] = 2;
			m[i][j+2] = 2;
			m[i][j-1] = 1;
			m[i][j+1] = 1;
		}
		
		for(i=0; i < 3; i++) {
			for(j=0; j < 4; j++) {
			System.out.print(m[i][j]);
			}
			System.out.println(" ");
		}
		
		System.out.println("--------Matriz 2x2--------");

		
		for(i=0; i < 2; i++) {
			for(j=0; j < 2; j++) {
				if (i == j) {
					n[i][j] = 'A';
				}
				else {
					n[i][j] = 'Z';
				}
			}
		}
		
		for(i=0; i < 2; i++) {
			for(j=0; j < 2; j++) {
			System.out.print(n[i][j]);
			}
			System.out.println(" ");
		}


	}

}
