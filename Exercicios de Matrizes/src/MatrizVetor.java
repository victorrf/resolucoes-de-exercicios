import java.util.Scanner;

/**
 * @author Victor Reis (2017101820)
 *
 */

public class MatrizVetor {

	static int i, j, somal, somac;
	static int g[][] = new int[3][3];
	static int sl[] = new int [3];
	static int sc[] = new int [3];
	static Scanner conta = new Scanner (System.in);
	
	public static void main(String[] args) {
		
		for(i=0; i < 3; i++) {
			for(j=0; j < 3; j++) {
				System.out.println("Digite um numero inteiro na Matriz D 3x3: ");
				g[i][j] = conta.nextInt();
			}
		}
		
		for(i=0; i < 3; i++){
			somal = 0;
			for(j=0; j < 3; j++) {
				somal+=g[i][j];
			}
			sl[i]=somal;
		}
		
		for(i=0; i < 3; i++){
			somac = 0;
			for(j=0; j < 3; j++) {
				somac+=g[j][i];
			}
			sc[i]=somac;
		}
		
		System.out.println("-----Vetor da soma das linhas-----");
		
		for(i=0; i < 3; i++) {
			System.out.print(sl[i]);
		}
		
		System.out.println(" ");
		System.out.println("-----Vetor da soma das Colunas-----");
		
		for(i=0; i < 3; i++) {
			System.out.print(sc[i]);
		}

	}

}
