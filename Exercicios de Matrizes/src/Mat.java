

/**
 * @author Victor Reis (2017101820)
 *
 */

public class Mat {
	
	static int i, j;
	static char mat [][] = { { 'O', 'Q', '*', 'I'},
							 { 'E', '*', 'E', 'S'},
							 { 'R', 'E', 'U', 'T'},
							 { 'A', '*', '*', 'S'}
						   };
	static char aux;


	public static void main(String[] args) {
		
		System.out.println("ANTES");
		System.out.println("-----Matriz 4x4-----");
		
		for(i=0; i < 4; i++) {
			for(j=0; j < 4; j++) {
				System.out.print(mat[i][j]);
			}
			System.out.println(" ");
		}
		
		for(i=0; i < 3; i++) {
			for(j=i+1; j < 4; j++) {
				aux = mat[i][j];
				mat[i][j] = mat[j][i];
				mat[j][i] = aux;
			}
		}
		
		aux = mat[0][0];
		mat[0][0] = mat[3][3];
		mat[3][3] = aux;
		aux = mat[1][1];
		mat[1][1] = mat[2][2];
		mat[2][2] = aux;
		
		System.out.println("DEPOIS");
		System.out.println("-----Matriz 4x4-----");
		
		for(i=0; i < 4; i++) {
			for(j=0; j < 4; j++) {
				System.out.print(mat[i][j]);
			}
			System.out.println(" ");
		}

	}

}
