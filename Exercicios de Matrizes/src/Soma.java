import java.util.Scanner;

/**
 * @author Victor Reis (2017101820)
 *
 */
public class Soma {
	
	static int i, j, SomaTotal, SomaLinha, SomaColuna;
	static int soma[][] = new int[4][4];
	static Scanner conta = new Scanner (System.in);

	public static void main(String[] args) {
		
		for(i=0; i < 4; i++) {
			for(j=0; j < 4; j++) {
				System.out.println("Digite um numero inteiro na Matriz D 3x3: ");
				soma[i][j] = conta.nextInt();
				SomaTotal+=soma[i][j];
			}
		}
		
		for(j=0; j < 4; j++) {
			SomaLinha+= soma[2][j];
		}
		
		for(i=0; i < 4; i++) {
			SomaColuna+= soma[i][1];
		}
		
		System.out.println("Soma de todos os elementos da linha 3 �: " + SomaLinha);
		System.out.println("Soma de todos os elementos da coluna 2 �: " + SomaColuna);
		System.out.println("Soma de todos os elementos da Matriz �: " + SomaTotal);

	}

}
